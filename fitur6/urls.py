from django.conf.urls import url
from .views import index, infobea

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^infobea/$', infobea, name='infobea'),
]
