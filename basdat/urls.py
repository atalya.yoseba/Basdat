"""basdat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import fitur5.urls as fitur5

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^fitur5', include(('fitur5.urls','fitur5'), namespace='fitur5')),
    url(r'^fitur6', include(('fitur6.urls','fitur6'), namespace='fitur6')),
    url(r'^fitur3', include(('fitur3.urls','fitur3'), namespace='fitur3')),
    url(r'^fitur4', include(('fitur4.urls','fitur4'), namespace='fitur4')),
]
